# gauth

Creating `otpauth://totp/issuer:account?issuer=issuer&secret=secret` URLs,
6-digit OTPs and helpers for base32-encoding and -decoding secrets.

## Tested With

- Google Authenticator on iPhone
- Microsoft Authenticator on iPhone
- Spaces in issuer name and account name
- Commas, single quotes and spaces in account name

## Use

```txt
go get gitlab.com/lercher/gauth/totp
```

```txt
go doc totp

package totp // import "gitlab.com/lercher/gauth/totp"

func BuildURL(secretBytes []byte, accountName, issuerName string) *url.URL
func DecodeSecret(encodedSecret string) ([]byte, error)
func EncodeSecret(secretBytes []byte) string
func Generate(secretBytes []byte, unixtimestamp int64) int
func GenerateNow(encodedSecret string) (int, int64, error)
```

## QR Codes

To generate QR codes of the URIs containing
the secret for scanning with the mobile's camera
we recommend `gozxing` as shown in the
[test file at func TestQRCodeFile()](https://gitlab.com/lercher/gauth/-/blob/9e2b982a95a34bf9dd579f2a4e2b88eb8e503b0d/totp/totp_test.go#L60)
which writes a PNG file to disk.

```go
import (
	"image/png"
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/qrcode"
)

func TestQRCodeFile(t *testing.T) {
	f, err := os.Create("ItsaMeMario.png")
	uri := BuildURL(secretBytes, accountName, issuerName).String()
	err = createQR(f, uri, 128+64)
	err = f.Close()
}

func createQR(w io.Writer, data string, size int) error {
	qrw := qrcode.NewQRCodeWriter()

	matrix, err := qrw.EncodeWithoutHint(data, gozxing.BarcodeFormat_QR_CODE, size, size)
	if err != nil {
		return fmt.Errorf("qr encode: %v", err)
	}

	err = png.Encode(w, matrix)
	if err != nil {
		return fmt.Errorf("qr png encode: %v", err)
	}

	return nil
}
```

This one:

![QR-Code](totp/ItsaMeMario.png)

`otpauth://totp/Martin%20Lercher%20Personal%20Systems%20OHG:It%27s%20a%20me,%20Mario?issuer=Martin%20Lercher%20Personal%20Systems%20OHG&secret=NV4VGZLDOJSXI`

## Important

**Keep your secrets secret!** Don't publish them or the QR codes unless it's needed
to couple an account to an authenticator app.
