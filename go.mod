module gitlab.com/lercher/gauth

go 1.21.0

require github.com/makiuchi-d/gozxing v0.1.1

require (
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
