package totp

// inspired by https://github.com/xlzd/gotp/blob/master/utils.go
// and https://rednafi.com/go/totp_client/
// https://github.com/google/google-authenticator/wiki/Key-Uri-Format

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base32"
	"encoding/binary"
	"net/url"
	"strings"
	"time"
)

const otpType = "totp"

var base32enc = base32.StdEncoding.WithPadding(base32.NoPadding)

// EncodeSecret creates the recommended base32 representation of some bytes.
func EncodeSecret(secretBytes []byte) string {
	return base32enc.EncodeToString(secretBytes)
}

// DecodeSecret decodes a base32 string to some bytes
func DecodeSecret(encodedSecret string) ([]byte, error) {
	// The base32 encoded secret key string is decoded to a byte slice
	secretKey := strings.ToUpper(strings.TrimSpace(encodedSecret)) // preprocess
	return base32enc.DecodeString(secretKey)                       // decode
}

// BuildURL returns an otpauth URL containing a secret that can be imported to an
// Authenticator app. It only supports default values for common auth apps as of 2023.
func BuildURL(secretBytes []byte, accountName, issuerName string) *url.URL {
	q := url.Values{}

	// issuer
	label := accountName
	if issuerName != "" {
		q.Set("issuer", issuerName)
		label = issuerName + ":" + accountName
	}

	// secret
	q.Set("secret", EncodeSecret(secretBytes))

	// defaults
	// q.Set("algorithm", "SHA1")
	// q.Set("digits", "6")
	// q.Set("period", "30") // for totp only
	// q.Set("counter", fmt.Sprintf("%d", initialCount)) // for hotp only

	qy := q.Encode()
	qy = strings.ReplaceAll(qy, "+", "%20") // nearly correct b/c the spec assumes spaces as %20 instead of +.

	u := &url.URL{
		Scheme:   "otpauth",
		Host:     otpType,
		Path:     label,
		RawQuery: qy,
	}
	return u
}

// GenerateNow returns the current OTP given a base32-encoded secret
func GenerateNow(encodedSecret string) (int, int64, error) {
	de, err := DecodeSecret(encodedSecret)
	if err != nil {
		return 0, 0, err
	}

	ts := time.Now().Unix()
	otp := Generate(de, ts)
	return otp, ts, nil
}

// Generate creates the default 6-digit, sha1 OTP
// based on the secret and a timestamp.
// unixtimestamp is often time.Now().Unix() and secretBytes can
// be obtained from its base32 form by DecodeSecret.
func Generate(secretBytes []byte, unixtimestamp int64) int {
	// The truncated timestamp / 30 is converted to an 8-byte big-endian
	// unsigned integer slice
	timeBytes := make([]byte, 8)
	binary.BigEndian.PutUint64(timeBytes, uint64(unixtimestamp)/30)

	// The timestamp bytes are concatenated with the decoded secret key
	// bytes. Then a 20-byte SHA-1 hash is calculated from the byte slice
	hash := hmac.New(sha1.New, secretBytes)
	hash.Write(timeBytes) // Concat the timestamp byte slice
	h := hash.Sum(nil)    // Calculate 20-byte SHA-1 digest

	// AND the SHA-1 with 0x0F (15) to get a single-digit offset
	offset := h[len(h)-1] & 0x0F

	// Truncate the SHA-1 by the offset and convert it into a 32-bit
	// unsigned int. AND the 32-bit int with 0x7FFFFFFF (2147483647)
	// to get a 31-bit unsigned int.
	truncatedHash := binary.BigEndian.Uint32(h[offset:]) & 0x7FFFFFFF

	// Take modulo 1_000_000 to get a 6-digit code
	return int(truncatedHash % 1_000_000)
}
