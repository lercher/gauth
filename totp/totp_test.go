package totp

import (
	"fmt"
	"io"
	"os"
	"strings"
	"testing"
	"time"

	"image/png"

	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/qrcode"
)

var (
	secretBytes = []byte("mySecret")
	accountName = "It's a me, Mario"
	issuerName  = "Martin Lercher Personal Systems OHG"
)

func TestEncDec(t *testing.T) {
	want := fmt.Sprint(secretBytes)
	en := EncodeSecret(secretBytes)
	de, err := DecodeSecret(en)
	if err != nil {
		t.Fatal(err)
	}
	got := fmt.Sprint(de)

	if got != want {
		t.Errorf("got %v, want %v", got, want)
	}
}

func TestDecEnc(t *testing.T) {
	want := "NV4VGZLDOJSXI"
	have := "  " + strings.ToLower(want) + " "
	de, err := DecodeSecret(have)
	if err != nil {
		t.Fatal(err)
	}

	got := EncodeSecret(de)

	if got != want {
		t.Errorf("have %q, got %v, want %v", have, got, want)
	}
}

func TestBuildURL(t *testing.T) {
	want := `otpauth://totp/Martin%20Lercher%20Personal%20Systems%20OHG:It%27s%20a%20me,%20Mario?issuer=Martin%20Lercher%20Personal%20Systems%20OHG&secret=NV4VGZLDOJSXI`
	got := BuildURL(secretBytes, accountName, issuerName).String()
	if got != want {
		t.Errorf("BuildURL() got %v, want %v", got, want)
	}
}

func TestQRCodeFile(t *testing.T) {
	f, err := os.Create("ItsaMeMario.png")
	if err != nil {
		t.Fatal(err)
	}

	uri := BuildURL(secretBytes, accountName, issuerName).String()
	err = createQR(f, uri, 128+64)
	if err != nil {
		t.Error(err)
	}

	err = f.Close()
	if err != nil {
		t.Fatal(err)
	}
}

func createQR(w io.Writer, data string, size int) error {
	qrw := qrcode.NewQRCodeWriter()

	matrix, err := qrw.EncodeWithoutHint(data, gozxing.BarcodeFormat_QR_CODE, size, size)
	if err != nil {
		return fmt.Errorf("qr encode: %v", err)
	}

	err = png.Encode(w, matrix)
	if err != nil {
		return fmt.Errorf("qr png encode: %v", err)
	}

	return nil
}

func TestGenerateA(t *testing.T) {
	var ts int64 = 1693754831 // time.Now().Unix()
	want := 227030

	got := Generate(secretBytes, ts)
	if got != want {
		t.Errorf("at timestamp %v: got %v, want %v", ts, got, want)
	}
}

func TestGenerateB(t *testing.T) {
	var ts int64 = 1693754831 + 30 // time.Now().Unix()
	want := 670096

	got := Generate(secretBytes, ts)
	if got != want {
		t.Errorf("at timestamp %v: got %v, want %v", ts, got, want)
	}
}

func TestGenerateNow(t *testing.T) {
	var ts int64 = time.Now().Unix()

	got := Generate(secretBytes, ts)
	t.Logf("at timestamp now, %v: got %v", ts, got)
}
