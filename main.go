package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/lercher/gauth/totp"
)

func main() {
	log.Println("This is gauth. It shows a 6-digit OTP based on a base32 encoded secret.")
	if 2 != len(os.Args) {
		log.Fatalln("usage:", os.Args[0], "<base32-secret>")
	}

	otp, ts, err := totp.GenerateNow(os.Args[1])
	if err != nil {
		log.Fatalln("error:", err)
	}
	log.Println("OTP for timestamp", ts, "is", otp)
	fmt.Println(otp)
}
